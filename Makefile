#!/usr/bin/make -f

# Copyright (c) 2017 Franco Masotti.
# See LICENSE file for details.

default: install-dev

install-dev:
	pipenv install --dev
	pipenv run pre-commit install
	pipenv graph
	pipenv check

uninstall-dev:
	rm -f Pipfile.lock
	pipenv --rm

update:
	pipenv run pre-commit autoupdate

.PHONY: default install-dev uninstall-dev
